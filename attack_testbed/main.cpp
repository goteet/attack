#include <common/include/memory.h>
#pragma comment(lib, "common.lib")
#pragma comment(lib, "attack.lib")
#pragma comment(lib, "file.lib")
#pragma comment(lib, "sheet.lib")

#include <locale>
#include <iostream>
#include <time.h>

#include "attack/include/property.h"
#include "attack/include/item.h"
#include "attack/source/util.h"

prop_character character;

void attack()
{
	damage d = character.m_attack.create_dmg();
	

	switch (character.m_misc.main_attrib_type)
	{
	case attrib_strength: std::cout << "强健的";		break;
	case attrib_dexterity: std::cout << "灵巧的";	break;
	case attrib_intelligent: std::cout << "机智的";	break;
	case attrib_vitality: std::cout << "雄壮的";		break;
	}
	std::cout << "你 --==> 史莱姆\n  造成了";
	if (d.m_damage[dmg_physic] > 0)
		std::cout << d.m_damage[dmg_physic] << "点物理伤害\n";
	if (d.m_damage[dmg_fire] > 0)
		std::cout << d.m_damage[dmg_fire] << "  点火焰伤害。\n";
	if (d.m_damage[dmg_cold] > 0)
		std::cout << d.m_damage[dmg_cold] << "  点冰冷伤害。\n";
	if (d.m_damage[dmg_lighting] > 0) 
		std::cout << d.m_damage[dmg_lighting] << "  点闪电伤害。\n";
	if (d.m_damage[dmg_poison] > 0)
		std::cout << d.m_damage[dmg_poison] << "  点毒素伤害。\n";
	if (d.crushing > 0)
	{
		std::cout << "    同时你力劈华山的攻势，让史莱姆深受重伤，消减了" << d.crushing*100.0f << "%的生命值。\n";
	}
	if (d.range > 0)
	{
		std::cout << "    并且溅开的石块对周围怪物造成了" << d.range << "点伤害。";
	}
	std::cout << std::endl << std::endl;
}

void defense_func()
{

	damage dmg;

	dmg.m_damage[dmg_physic] = rand() % 2 * 1.0f;
	dmg.m_damage[dmg_fire] = rand() % 11 * 1.0f;
	if (dmg.m_damage[dmg_physic] == 0 && dmg.m_damage[dmg_fire] == 0)
	{
		std::cout << "史莱姆正在发呆地看着你" << std::endl;

	}
	else
	{
		defense def = character.m_defense.receive_dmg(dmg);

		std::cout << "你 <==-- 史莱姆\n";
		if (dmg.m_damage[dmg_physic] > 0)
		{
			if (dmg.m_damage[dmg_fire] > 0)
			{
				std::cout << "  发动附带" << dmg.m_damage[dmg_fire] << "点火焰伤害的\n  攻击力为"
					<< dmg.m_damage[dmg_physic] << "的法术攻击\n";
			}
			else
			{
				std::cout << "    发动攻击力为" << dmg.m_damage[dmg_physic] << "的物理攻击。\n";
			}

			if (def.is_dodged)
			{
				std::cout << "    被你闪避掉了。\n";
			}
			else
			{
				if (def.is_blocked)
					std::cout << "    被你挡格了。\n";
				std::cout << "    造成了" << def.m_damage[dmg_physic] + def.m_damage[dmg_fire] << "点伤害, \n";
				std::cout << "    你的护甲对怪物造成了" << def.thorns << "点的荆棘伤害, \n";
			}
		}
		else
		{
			std::cout << "  释放攻击力为" << dmg.m_damage[dmg_fire] << "的技能。\n  造成了" << def.m_damage[dmg_fire] << "点伤害\n";
		}
		std::cout << std::endl << std::endl;

	}
}

int main_palette()
{
	srand(static_cast<unsigned>(time(0)));

	character.m_attack.main_attrib = 10;

	for (long i = 0;; i++)
	{
		attack();
		defense_func();
		getchar();
		system("cls");
	}
	return 0;
}

int main()
{
	setlocale(LC_ALL, "chs");
	srand(static_cast<unsigned>(time(0)));

	const int ITEM_COUNT = 1;
	for (long i = 0; ; i++)
	{
		//改变主属性
		character.m_misc.main_attrib_type = (attrib_type)rand_range((int)attrib_strength, (int)attrib_vitality);

		item tmp_item[ITEM_COUNT];
		for (int i = 0; i < ITEM_COUNT; i++)
		{
			tmp_item[i].create();
			tmp_item[i].on_equip(&character);

			std::cout << "穿上装备：\n";
			for (cmn::byte j = 0; j < tmp_item[i].m_entry_count; j++)
			{
				std::wcout << tmp_item[i].m_entrys[j]->get_desc().c_str();
			}
			std::wcout << std::endl;
		}

		attack();
		defense_func();

		getchar();
		system("cls");

		for (int i = 0; i < ITEM_COUNT; i++)
		{
			tmp_item[i].on_unequip(&character);
			tmp_item[i].destroy();
		}
	}
	return 0;
}