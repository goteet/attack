#pragma once

#include <file/include/file.h>
#include <string>
#include <map>

struct item
{
	int		id			= 0;
	bool	is_pair		= false;
	int		type		= 0;
	int		level		= 1;
	int		min			= 0;
	int		max			= 1;

	std::wstring name	= L"name";
	std::wstring desc	= L"desc";
	std::wstring memo	= L"none";
	
	void write(io::ofile* _file);
};

class item_list
{
public:
	void write(io::ofile* _file);

//private:
	std::map<int, item*> m_items;
};