
// main_frm.h : main_frm ��Ľӿ�
//

#pragma once
#include "view.h"

class main_frm : public CFrameWnd
{
public:
	main_frm();

	virtual ~main_frm();

	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	virtual BOOL OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo);

#ifdef _DEBUG
	virtual void AssertValid() const;

	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	CToolBar	m_wndToolBar;
	CStatusBar	m_wndStatusBar;
	view		m_wndView;

	DECLARE_DYNAMIC(main_frm)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSetFocus(CWnd *pOldWnd);
	DECLARE_MESSAGE_MAP()

};


