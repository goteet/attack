#include "stdafx.h"
#include "../include/item.h"

void item::write(io::ofile* _file)
{
	
	wchar_t temp[2048];

	swprintf(temp, L"%d\t%d\t%d\t%s\t%d\t%d\t%d\t%s\t%s\n", id, (is_pair?1:0), type, name.c_str(), level, min, max, desc.c_str(), memo.c_str());
	std::wstring line = temp;

	io::write(_file, line.c_str());
}

void item_list::write(io::ofile* _file)
{
	//file head
	cmn::word bom = BOM;
	io::write(_file, bom);

	//title
	std::wstring title = L"id|I\tformat|N\ttype|N\tname|S\tlevel|N\tmin|N\tmax|N\tdesc|S\tmemo|S\n";
	io::write(_file, title.c_str());

	//entry
	int item_count = m_items.size();
	auto cur = m_items.begin();
	auto end = m_items.end();
	while ( cur != end )
	{
		cur->second->write(_file);
		cur++;
	}
}
