
// entry_template_editor.h : entry_template_editor 应用程序的主头文件
//
#pragma once

#ifndef __AFXWIN_H__
	#error "在包含此文件之前包含“stdafx.h”以生成 PCH 文件"
#endif

#include "resource.h"

class app : public CWinApp
{
public:
	app();

	virtual BOOL InitInstance();

	virtual int ExitInstance();
};

extern app theApp;
