#ifndef item_h
#define item_h

#include <string>
#include <map>
#include <common/include/typedef.h>
#include <common/include/interface.h>
enum item_entry_type{
	//����˳��Ҫ��������palette˳���Ӧ�ϵ�

	//Ԫ�ع�����׺
	ie_dmg_physic = 0,
	ie_dmg_fire,
	ie_dmg_cold,
	ie_dmg_lighting,
	ie_dmg_poison,
	ie_dmg_end,

	//Ԫ����ǿ��׺
	ie_elerate_physic,
	ie_elerate_fire,
	ie_elerate_cold,
	ie_elerate_lighting,
	ie_elerate_poison,
	ie_elerate_end,

	//Ԫ�ؿ��ƴ�׺
	ie_electrl_elite_rate,
	ie_electrl_blind_rate,
	ie_electrl_freeze_rate,
	ie_electrl_erose_rate,
	ie_electrl_slow_rate,
	ie_electrl_end,

	//������
	ie_attrib_strength,
	ie_attrib_dexterity,
	ie_attrib_intelligent,
	ie_attrib_vitality,
	ie_attrib_str_vit,
	ie_attrib_dex_vit,
	ie_attrib_int_vit,
	ie_attrib_end,

	//������׺
	ie_crit_chance,
	ie_crit_rate,
	ie_crushing_chance,
	ie_crushing_rate,
	ie_area_chance,
	ie_area_rate,
	ie_attack_speed,
	ie_attack_rate,

	//������׺
	ie_all_reduce,
	ie_armor,
	ie_res_all,
	ie_res_physic,
	ie_res_fire,
	ie_res_cold,
	ie_res_lighting,
	ie_res_poison,
	ie_block_chance,
	ie_block_rate,
	ie_block_value,
	ie_dodge_chance,

	//������׺
	ie_life_value_per_hit,	
	ie_life_rate_per_hit,
	ie_life_value_per_sec,
	ie_life_rate_per_sec,
	ie_life_inc_rate,
	ie_life_inc_value,
	ie_life_per_kill,

	//������׺
	ie_energy_value_inc,
	ie_energy_value_per_sec,
	ie_energy_rate_per_sec,
	ie_energy_value_per_hit,
	ie_energy_rate_per_hit,
	ie_energy_reduce_rate,
	ie_energy_reduce_value,

	//���ܴ�׺
	ie_skill_cdreduce_rate,
	ie_skill_cdreduce_sec,
	ie_skill_1,

	//ie_erose_

	//������׺
	ie_socket,
	ie_inc_range,	//������Χ��ʰȡ��Χ�����ܷ�Χ�����ӷ�Χ
	ie_thorn_rate,
	ie_thorn_value,

	ie_cnt,
};

enum item_entry_class_type{
	iec_avr_dmg = 0,
	iec_element_enhance,
	iec_main_attrib,
	iec_attack,
	iec_defense,
	iec_life,
	iec_energy,
	iec_skill,
	iec_misc,
	iec_type_count,
};

class item_entry;
class item_entry_template;
class prop_character;

class item_entry : public cmn::interface_t
{
public:
	virtual cmn::dword release();

	item_entry(item_entry_template* _template){ m_template = _template; }

	void on_equip(prop_character*);

	void on_unequip(prop_character*);

	std::wstring get_desc();

	item_entry_template* m_template;

	int m_value;
};

class item_entry_template : public cmn::interface_t
{
public:
	virtual ~item_entry_template(){}

	virtual std::wstring	get_name()		= 0;

	virtual std::wstring	get_desc(item_entry*) = 0;

	virtual item_entry_type	get_type()		= 0;

	virtual int				get_level()		= 0;

	virtual item_entry*	create_entry()		= 0;

	virtual item_entry*	create_entry(int _v)= 0;

	virtual void on_equip(item_entry*, prop_character*)	= 0;

	virtual void on_unequip(item_entry*, prop_character*)= 0;
};

class item_entry_dic
{
public:
	item_entry_dic();

	~item_entry_dic();

	item_entry* create_entry(item_entry_type _type, int _level);

	struct entry_range_def{
		item_entry_class_type	type;
		item_entry_type			begin;
		item_entry_type			end;
	} class_list[iec_type_count];

private:
	struct entry_key{
		cmn::dword type;
		cmn::dword level;
		bool operator<(const entry_key& _rhs) const {
			if (type < _rhs.type)
				return true;
			if (level < _rhs.level)
				return true;
			return false;
		}
	};
	std::map<entry_key, item_entry_template*> m_dictionary;
};

extern item_entry_dic  g_item_entry_dic;


class item
{
public:
	item();

	~item();

	bool create();

	bool load();

	void destroy();
	
	void on_equip(prop_character*);

	void on_unequip(prop_character*);

	enum{ENTRY_DMG_ARM =0, ENTRY_ATT_SPEED=1, MAX_ENTRY_COUNT=10};
	item_entry*	m_entrys[MAX_ENTRY_COUNT];
	cmn::byte	m_entry_count;
};
#endif	//item_h
