#ifndef prop_h
#define prop_h

enum dmg_type{
	dmg_physic = 0,	//普通
	dmg_fire,		//溅射，几率致盲
	dmg_cold,		//减速,几率冰冻
	dmg_lighting,	//高伤害，几率眩晕
	dmg_poison,		//dot，几率破甲
	dmg_type_cnt,
};

enum attrib_type{
	attrib_strength = 0,	//力量
	attrib_dexterity,		//敏捷
	attrib_intelligent,		//智力
	attrib_vitality,		//体能
	attrib_type_cnt,
};
struct damage_seed
{
	float get_dmg();

	int min = 0;
	int max = 0;
};

struct type_palette
{
	type_palette();

	type_palette(const type_palette& rhs);

	type_palette& operator=(const type_palette& rhs);
	
	float& operator[](int _index);

	const float& operator[](int _index) const;

	float get_sum();

	float m_value[dmg_type_cnt];	//数值伤害
private:
	void copy(const type_palette& rhs);
};

struct damage
{
	type_palette m_damage;
	float range		= 0;	//数值伤害
	float crushing	= 0;	//比例伤害
};

struct defense
{
	type_palette m_damage;
	float thorns	= 0.0f;
	bool is_dodged	= false;
	bool is_blocked	= false;
};

class prop_attack
{
public:
	prop_attack();

	damage create_dmg();

	damage_seed	dmg_seed[dmg_type_cnt];	//种子
	type_palette enhance;		//属性强化
	float main_attrib;			//主属性
	float critical_chance;		//暴击几率
	float critical_dmg;			//暴击倍率
	float crushinging_chance;	//压碎几率
	float crushinging_rate;		//压碎比例
	float range_chance;			//溅射几率
	float range_rate;			//溅射比例
	float range_radius;			//溅射范围

private:
	float get_main_attrib_scale();
	float get_critical_scale();
	float get_range_scale();
	float get_crushing_blow_scale();
};

class prop_defense
{
public:
	prop_defense();
	
	defense receive_dmg(const damage& _dmg);

	type_palette res_rate;	//百分比抗性--物理护甲
	type_palette res_value;	//数值抗性--物理护甲
	float extra_reduce;		//额外减伤
	float block_chance;		//挡格几率
	float block_rate;		//挡格减伤比例
	float block_value;		//挡格减伤数值
	float dodge_chance;		//闪避几率
	float thorns_value;		//荆棘数值伤害
	float thorns_rate;		//荆棘比例伤害

private:
	bool	is_dodged();
	bool	is_block();	
	float	get_reduce_dmg(dmg_type _type);
	float	get_res_reduce_rate(dmg_type _type);
	float	get_reduce_by_block();
	float	get_block_reduce_rate();
};

class prop_life
{
public:
	float vitality;
	float rate_per_hit;		//类似吸血
	float value_per_hit;
	float rate_per_sec;
	float value_per_sec;
	float inc_rate;
	float inc_value;
	float value_per_enegery;
	float value_per_kill;
};

class prop_energy
{
public:
	float value_inc;
	float rate_inc;
	float reduce_rate;
	float reduce_value;
	float value_per_sec;
	float rate_per_sec;
	float value_per_hit;
	float rate_per_hit;		//能量吸取
};

class prop_skill
{
public:
	float cd_reduce_rate;
	float cd_reduce_sec;
};

class prop_misc
{
public:
	float eliete_rate;
	float blind_rate;
	float freeze_rate;
	float stun_rate;
	float erose_rate;

	float slow_rate;
	float burn_sec;
	float stun_sec;

	attrib_type main_attrib_type = attrib_strength;
};

class prop_character
{
public:
	prop_attack		m_attack;
	prop_defense	m_defense;
	prop_life		m_life;
	prop_energy		m_energy;
	prop_skill		m_skill;
	prop_misc		m_misc;
};

#endif	//prop_h

