#ifndef util_h
#define util_h

#include <cmath>

inline float rand01()
{
	return rand() * 1.0f / RAND_MAX;
}

inline float rand_scale(float _range)
{
	return _range * rand01();
}

template <typename T>
inline T rand_range(T _min, T _max)
{
	return _min + static_cast<T>(rand_scale((_max - _min) * 1.0f) + 0.5f);
}

template <>
inline float rand_range(float _min, float _max)
{
	return _min + rand_scale(_max - _min);
}

template <>
inline double rand_range(double _min, double _max)
{
	return _min + rand_scale((float)_max - (float)_min);
}

inline bool is_trigger(float _rate)
{
	float sample = rand() * 1.0f / RAND_MAX;
	return (sample <= _rate);
}
#endif