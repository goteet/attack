#include "../include/property.h"
#include <cstdlib>
#include "util.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 基础伤害
float damage_seed::get_dmg()
{
	int range	= max - min;

	return  min + rand01() * range;
};


float type_palette::get_sum()
{
	float result = 0.0f;
	for (int i = 0; i < dmg_type_cnt; i++)
		result += m_value[i];
	return result;
}

type_palette::type_palette()
{
	for (int i = 0; i < dmg_type_cnt; i++)
		m_value[i] = 0.0f;
}

type_palette::type_palette(const type_palette& rhs)
{
	copy(rhs);
}

type_palette& type_palette::operator=(const type_palette& rhs)
{
	copy(rhs);
	return *this;
}

void type_palette::copy(const type_palette& rhs)
{
	for (int i = 0; i < dmg_type_cnt; i++)
		m_value[i] = rhs.m_value[i];
}

float& type_palette::operator[](int _index)
{
	return m_value[_index];
}

const float& type_palette::operator[](int _index) const
{
	return m_value[_index];
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 攻击
prop_attack::prop_attack()
{
	main_attrib			= 0.0f;
	critical_chance		= 0.05f;
	critical_dmg		= 0.2f;
	crushinging_chance	= 0.5f;
	crushinging_rate	= 0.02f;
	range_chance		= 0.05f;
	range_rate			= 0.5f;
	range_radius		= 5.0f;
}

damage prop_attack::create_dmg()
{
	damage dmg;

	float attrib_scale		= 1.0f + get_main_attrib_scale();
	float critical_scale	= 1.0f + get_critical_scale();

	for (int i = 0; i < dmg_type_cnt; i++)
	{
		dmg.m_damage[i] = dmg_seed[i].get_dmg();
		float e = critical_scale * attrib_scale * (1.0f + enhance[i]);
		dmg.m_damage[i] *= e;

		dmg.range += dmg.m_damage[i];
	}

	dmg.crushing = get_crushing_blow_scale();
	dmg.range *= get_range_scale();

	return dmg;
}

float prop_attack::get_main_attrib_scale()
{
	return main_attrib * 0.1f;
}

float prop_attack::get_critical_scale()
{
	return is_trigger(critical_chance) ? critical_dmg : 0.0f;
}

float prop_attack::get_range_scale()
{
	return is_trigger(range_chance) ? range_rate : 0.0f;
}

float prop_attack::get_crushing_blow_scale()
{
	return is_trigger(crushinging_chance) ? crushinging_rate : 0.0f;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 防御
prop_defense::prop_defense()
{
	for (int i = 0; i < dmg_type_cnt; i++)
	{
		res_rate[i]	= 0.0f;
		res_value[i]= 0.0f;
	}

	extra_reduce	= 0.01f;
	block_chance	= 0.0f;
	block_rate	= 0.05f;
	block_value		= 0.0f;
	dodge_chance	= 0.05f;
	thorns_value	= 1.0f;
	thorns_rate		= 0.0f;
}

defense prop_defense::receive_dmg(const damage& _dmg)
{
	defense def;
	def.m_damage = _dmg.m_damage;

	if (def.m_damage[dmg_physic] > 0)
	{
		def.is_dodged = is_dodged();
		if (def.is_dodged)
		{
			return def;
		}

		//只有存在物理伤害的部分才会被挡格和闪避。
		def.is_blocked = is_block();
		def.thorns = def.m_damage[dmg_physic] * thorns_rate + thorns_value;
	}

	float er = 1.0f - extra_reduce;
	float br = def.is_blocked ? get_reduce_by_block() : 1.0f;
	for (int i = 0; i < dmg_type_cnt; i++)
	{
		
		def.m_damage[i] *= er * get_reduce_dmg((dmg_type)i);
		if (i == dmg_physic)
			def.m_damage[i] *= br;
	}
	return def;
}

bool prop_defense::is_dodged()
{
	return is_trigger(dodge_chance);
}

bool prop_defense::is_block()
{
	return is_trigger(block_chance);
}

float prop_defense::get_reduce_dmg(dmg_type _type)
{
	return __max(1.0f - res_rate[_type] - get_res_reduce_rate(_type), 0.0f);
}

float prop_defense::get_res_reduce_rate(dmg_type _type)
{
	//res_value[_type]
	return 0.0f;
}

float prop_defense::get_reduce_by_block()
{
	return __max(1.0f - block_rate - get_block_reduce_rate(), 0.0f);
}

float prop_defense::get_block_reduce_rate()
{
	//block_value
	return 0.0f;
}