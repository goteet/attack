#include "../include/item.h"
#include "util.h"
#include "../include/property.h"
#include <algorithm>

#include <sheet/include/sheet.h>

#pragma warning(push)
#pragma warning(disable:4996)

namespace {

	class pair_base_template : public item_entry_template
	{
	public:
		virtual cmn::dword release()
		{
			delete this;
			return 0;
		}

		virtual item_entry* create_entry()
		{
			item_entry* entry = new item_entry(this);
			short min = rand_range(min_min, min_max);
			short max = rand_range(max_min, max_max);
			entry->m_value = (max << 16) | min;
			return entry;
		}

		virtual item_entry*	create_entry(int _v)
		{
			item_entry* entry = create_entry();
			entry->m_value = _v;
			return entry;
		}

		short get_min(int _v)	{ return _v & 0xff;}

		short get_max(int _v)	{ return (_v>>16) & 0xff; }

		short min_min = 0;
		short min_max = 0;
		short max_min = 0;
		short max_max = 0;
	};

	class single_base_template : public item_entry_template
	{
	public:
		virtual cmn::dword release()
		{
			delete this;
			return 0;
		}

		virtual item_entry* create_entry()
		{
			item_entry* entry = new item_entry(this);
			entry->m_value = rand_range(min, max);
			return entry;
		}

		virtual item_entry*	create_entry(int _v)
		{
			item_entry* entry = create_entry();
			entry->m_value = _v;
			return entry;
		}

		int min = 0;
		int max = 0;
	};
}

namespace sheet_entry_template{

	class base_entry_template : public item_entry_template
	{
	public:
		virtual cmn::dword release()
		{
			delete this;
			return 0;
		}

		base_entry_template(excel::entry* _entry)
		{
			m_level = _entry->get_cell(L"level")->to_int();
			m_desc	= _entry->get_cell(L"desc")->to_wstring();
			m_name = _entry->get_cell(L"name")->to_wstring();
			m_type = (item_entry_type)_entry->get_cell(L"type")->to_int();
		}

		virtual std::wstring get_name()
		{
			return m_name;
		}

		virtual item_entry_type	get_type()
		{
			return m_type;
		}

		virtual int	get_level()
		{
			return m_level;
		}

		virtual void on_equip(item_entry* _entry, prop_character* _char)
		{
			item_entry* entry = dynamic_cast<item_entry*>(_entry);
		}

		virtual void on_unequip(item_entry* _entry, prop_character* _char)
		{
			item_entry* entry = dynamic_cast<item_entry*>(_entry);
		}

		std::wstring m_desc;
		std::wstring m_name;
		int m_level;
		item_entry_type m_type;
	};

	class single_entry_template : public base_entry_template
	{
	public:

		single_entry_template(excel::entry* _entry) : base_entry_template(_entry)
		{
			m_min = _entry->get_cell(L"min")->to_int();
			m_max = _entry->get_cell(L"max")->to_int();
		}

		virtual item_entry* create_entry()
		{
			item_entry* entry = new item_entry(this);
			entry->m_value = rand_range(m_min, m_max);
			return entry;
		}

		virtual item_entry*	create_entry(int _v)
		{
			item_entry* entry = create_entry();
			entry->m_value = _v;
			return entry;
		}

		virtual std::wstring get_desc(item_entry* _entry)
		{
			wchar_t buffer[255];
			swprintf(buffer, m_desc.c_str(), _entry->m_value);
			return buffer;
		}

		int m_min = 0;
		int m_max = 0;
	};

	class pair_entry_template : public base_entry_template
	{
	public:
		pair_entry_template(excel::entry* _entry) : base_entry_template(_entry)
		{
			int min = _entry->get_cell(L"min")->to_int();
			int max = _entry->get_cell(L"max")->to_int();

			m_min_min = min & 0xff;
			m_min_max = (min >> 16) & 0xff;
			m_max_min = max & 0xff;
			m_max_max = (max >> 16) & 0xff;
		}

		virtual item_entry* create_entry()
		{
			item_entry* entry = new item_entry(this);
			short min = rand_range(m_min_min, m_min_max);
			short max = rand_range(m_max_min, m_max_max);
			entry->m_value = (max << 16) | min;
			return entry;
		}

		virtual item_entry*	create_entry(int _v)
		{
			item_entry* entry = create_entry();
			entry->m_value = _v;
			return entry;
		}

		short get_min(int _v)	{ return _v & 0xff; }

		short get_max(int _v)	{ return (_v >> 16) & 0xff; }

		virtual std::wstring get_desc(item_entry* _entry)
		{
			wchar_t buffer[255];
			swprintf(buffer, m_desc.c_str(),
				get_min(_entry->m_value),
				get_max(_entry->m_value));
			return buffer;
		}

		short m_min_min = 0;
		short m_min_max = 0;
		short m_max_min = 0;
		short m_max_max = 0;
	};
}

//////////////////////////////////////////////////////////////////////////
// 均伤词缀
class dmg_seed_entry_template : public  pair_base_template
{
public:
	dmg_seed_entry_template()
	{
		min_min = 5;
		min_max = 7;
		max_min = 8;
		max_max = 10;
	}

	virtual void on_equip(item_entry* _entry, prop_character* _char)
	{
		item_entry* entry = dynamic_cast<item_entry*>(_entry);

		short min = entry->m_value & 0xff;
		short max = (entry->m_value >> 16) & 0xff;

		dmg_type t = get_dmg_type();
		
		_char->m_attack.dmg_seed[t].min += min;

		_char->m_attack.dmg_seed[t].max += max;
		
	}

	virtual void on_unequip(item_entry* _entry, prop_character* _char)
	{
		item_entry* entry = dynamic_cast<item_entry*>(_entry);

		short min = entry->m_value & 0xff;
		short max = (entry->m_value >> 16) & 0xff;

		dmg_type t = get_dmg_type();

		_char->m_attack.dmg_seed[t].min -= min;
		_char->m_attack.dmg_seed[t].max -= max;
	}

	virtual std::wstring get_desc(item_entry* _entry)
	{
		wchar_t buffer[255];
		swprintf(buffer, L"增加%d-%d点%s\n",
			get_min(_entry->m_value),
			get_max(_entry->m_value),
			get_name().c_str());
		return buffer;
	}

protected:
	dmg_type get_dmg_type()
	{
		int index = get_type() - ie_dmg_physic;
		const dmg_type types[] = { dmg_physic, dmg_fire, dmg_cold, dmg_lighting, dmg_poison };
		return types[index];
	}
};

namespace dmg_seed_physic{

	class entry_template : public dmg_seed_entry_template
	{
	public:
		virtual std::wstring get_name()
		{
			return L"武器伤害";
		}

		virtual item_entry_type	get_type()
		{
			return ie_dmg_physic;
		}
	};
}

namespace dmg_seed_fire{

	class entry_template : public dmg_seed_entry_template
	{
	public:
		virtual std::wstring get_name()
		{
			return L"火焰伤害";
		}

		virtual item_entry_type	get_type()
		{
			return ie_dmg_fire;
		}
	};
}

namespace dmg_seed_cold{

	class entry_template : public dmg_seed_entry_template
	{
	public:
		virtual std::wstring get_name()
		{
			return L"冰冷伤害";
		}

		virtual item_entry_type	get_type()
		{
			return ie_dmg_cold;
		}
	};
}

namespace dmg_seed_lighting{

	class entry_template : public dmg_seed_entry_template
	{
	public:
		virtual std::wstring get_name()
		{
			return L"闪电伤害";
		}

		virtual item_entry_type	get_type()
		{
			return ie_dmg_lighting;
		}
	};
}

namespace dmg_seed_poison{

	class entry_template : public dmg_seed_entry_template
	{
	public:
		virtual std::wstring get_name()
		{
			return L"毒素伤害";
		}

		virtual item_entry_type	get_type()
		{
			return ie_dmg_poison;
		}
	};
}

//////////////////////////////////////////////////////////////////////////
// 主属性词缀
// 单属性
class single_attrib_entry_template : public  single_base_template
{
public:
	single_attrib_entry_template()
	{
		min = 50;
		max = 70;
	}

	virtual void on_equip(item_entry* _entry, prop_character* _char)
	{
		item_entry* entry = dynamic_cast<item_entry*>(_entry);

		attrib_type t = get_attrib_type();

		if (_char->m_misc.main_attrib_type == t)
		{
			_char->m_attack.main_attrib += _entry->m_value;
		}

	}

	virtual void on_unequip(item_entry* _entry, prop_character* _char)
	{
		item_entry* entry = dynamic_cast<item_entry*>(_entry);

		attrib_type t = get_attrib_type();

		if (_char->m_misc.main_attrib_type == t)
		{
			_char->m_attack.main_attrib -= _entry->m_value;
		}
	}

	virtual std::wstring get_desc(item_entry* _entry)
	{
		wchar_t buffer[255];
		swprintf(buffer, L"增加%d点%s\n", _entry->m_value, get_name().c_str());
		return buffer;
	}

protected:
	attrib_type get_attrib_type()
	{
		int index = get_type() - ie_attrib_strength;
		const attrib_type types[] = { attrib_strength, attrib_dexterity, attrib_intelligent, attrib_vitality };
		return types[index];
	}
};

class pair_attrib_entry_template : public  pair_base_template
{
public:
	pair_attrib_entry_template()
	{
		min_min = 20;
		min_max = 40;
		max_min = 20;
		max_max = 40;
	}

	virtual void on_equip(item_entry* _entry, prop_character* _char)
	{
		item_entry* entry = dynamic_cast<item_entry*>(_entry);

		attrib_type f,s;
		get_attrib_type(f,s);

		
		if (_char->m_misc.main_attrib_type == f)
		{
			_char->m_attack.main_attrib += get_min(_entry->m_value);
		}
		else if (_char->m_misc.main_attrib_type == s)
		{
			_char->m_attack.main_attrib += get_max(_entry->m_value);
		}
		
	}

	virtual void on_unequip(item_entry* _entry, prop_character* _char)
	{
		item_entry* entry = dynamic_cast<item_entry*>(_entry);

		attrib_type f, s;
		get_attrib_type(f, s);
		
		if (_char->m_misc.main_attrib_type == f)
		{
			_char->m_attack.main_attrib -= get_min(_entry->m_value);
		}
		else if (_char->m_misc.main_attrib_type == s)
		{
			_char->m_attack.main_attrib -= get_max(_entry->m_value);
		}
	}

	virtual std::wstring get_desc(item_entry* _entry)
	{
		wchar_t buffer[255];
		std::wstring first	= get_name().substr(0, 2);
		std::wstring second	= get_name().substr(2);

		swprintf(buffer, L"增加%d点%s\n增加%d点%s\n",
			get_min(_entry->m_value),
			first.c_str(),
			get_max(_entry->m_value),
			second.c_str()
		);
		return buffer;
	}

protected:
	
	void get_attrib_type(attrib_type& first_, attrib_type& second_)
	{
		int index = 2 * (get_type() - ie_attrib_str_vit);
		const attrib_type types[] = {
			attrib_strength, attrib_vitality,
			attrib_dexterity, attrib_vitality,
			attrib_intelligent, attrib_vitality
		};
		first_ = types[index];
		second_ = types[index+1];
	}
	
};

namespace attrib_entry_strength{

	class entry_template : public single_attrib_entry_template
	{
	public:
		virtual std::wstring get_name()
		{
			return L"力量";
		}

		virtual item_entry_type	get_type()
		{
			return ie_attrib_strength;
		}
	};
}

namespace attrib_entry_dexterity{

	class entry_template : public single_attrib_entry_template
	{
	public:
		virtual std::wstring get_name()
		{
			return L"敏捷";
		}

		virtual item_entry_type	get_type()
		{
			return ie_attrib_dexterity;
		}
	};
}

namespace attrib_entry_intelligent{

	class entry_template : public single_attrib_entry_template
	{
	public:
		virtual std::wstring get_name()
		{
			return L"智力";
		}

		virtual item_entry_type	get_type()
		{
			return ie_attrib_intelligent;
		}
	};
}

namespace attrib_entry_vitality{

	class entry_template : public single_attrib_entry_template
	{
	public:
		virtual std::wstring get_name()
		{
			return L"体能";
		}

		virtual item_entry_type	get_type()
		{
			return ie_attrib_vitality;
		}
	};
}

namespace attrib_entry_str_vit{

	class entry_template : public pair_attrib_entry_template
	{
	public:
		virtual std::wstring get_name()
		{
			return L"力量体能";
		}

		virtual item_entry_type	get_type()
		{
			return ie_attrib_str_vit;
		}
	};
}

namespace attrib_entry_dex_vit{

	class entry_template : public pair_attrib_entry_template
	{
	public:
		virtual std::wstring get_name()
		{
			return L"敏捷体能";
		}

		virtual item_entry_type	get_type()
		{
			return ie_attrib_dex_vit;
		}
	};
}

namespace attrib_entry_int_vit{

	class entry_template : public pair_attrib_entry_template
	{
	public:
		virtual std::wstring get_name()
		{
			return L"智力体能";
		}

		virtual item_entry_type	get_type()
		{
			return ie_attrib_int_vit;
		}
	};
}

void item_entry::on_equip(prop_character* _char)
{
	if (m_template)m_template->on_equip(this, _char);
}

void item_entry::on_unequip(prop_character* _char)
{
	if (m_template)m_template->on_unequip(this, _char);
}

std::wstring item_entry::get_desc()
{
	return m_template->get_desc(this);
}

cmn::dword item_entry::release()
{
	delete this;
	return 0;
}

item_entry_dic::item_entry_dic()
{
	item_entry_template* entry;

	//均伤
	int cl_id = 0;
	class_list[cl_id].type = iec_avr_dmg;
	class_list[cl_id].begin = ie_dmg_physic;
	class_list[cl_id].end = ie_dmg_end;
	cl_id++;
	/*
	entry = new dmg_seed_physic::entry_template();
	m_dictionary[entry->get_type()] = entry;

	entry = new dmg_seed_fire::entry_template();
	m_dictionary[entry->get_type()] = entry;

	entry = new dmg_seed_cold::entry_template();
	m_dictionary[entry->get_type()] = entry;

	entry = new dmg_seed_lighting::entry_template();
	m_dictionary[entry->get_type()] = entry;

	entry = new dmg_seed_poison::entry_template();
	m_dictionary[entry->get_type()] = entry;

	class_list[cl_id].type = iec_main_attrib;
	class_list[cl_id].begin = ie_attrib_strength;
	class_list[cl_id].end = ie_attrib_end;
	cl_id++;

	entry = new attrib_entry_strength::entry_template();
	m_dictionary[entry->get_type()] = entry;

	entry = new attrib_entry_dexterity::entry_template();
	m_dictionary[entry->get_type()] = entry;

	entry = new attrib_entry_intelligent::entry_template();
	m_dictionary[entry->get_type()] = entry;

	entry = new attrib_entry_vitality::entry_template();
	m_dictionary[entry->get_type()] = entry;

	entry = new attrib_entry_str_vit::entry_template();
	m_dictionary[entry->get_type()] = entry;

	entry = new attrib_entry_dex_vit::entry_template();
	m_dictionary[entry->get_type()] = entry;

	entry = new attrib_entry_int_vit::entry_template();
	m_dictionary[entry->get_type()] = entry;
	*/

 	excel::sheet* sh = excel::load_sheet("../resource/sheet.txt");
	if (sh)
	{
		auto cur = sh->first();
		auto end = sh->last();
		while ( !cur->is_equal(end) )
		{
			excel::entry* e = cur->get_entry();
			
			entry_key key;
			key.type	= e->get_cell(L"type")->to_int();
			key.level	= e->get_cell(L"level")->to_int();

			int format = e->get_cell(L"format")->to_int();
			
			if (format == 0)
			{
				item_entry_template* et = new sheet_entry_template::single_entry_template(e);
				m_dictionary[key] = et;
			}
			else
			{
				item_entry_template* et = new sheet_entry_template::pair_entry_template(e);
				m_dictionary[key] = et;
			}
			
			
			
			cur->next();
		}
		
		sh->release();
	}
	
}

item_entry_dic::~item_entry_dic()
{
	std::for_each(
		m_dictionary.begin(),
		m_dictionary.end(),
		[](const std::pair<entry_key, item_entry_template*>& _pair){
			_pair.second->release();
		} );
}

item_entry* item_entry_dic::create_entry(item_entry_type _type, int _level)
{
	entry_key key;
	key.type	= _type;
	key.level	= _level;


	auto it_find = m_dictionary.find(key);
	if (it_find != m_dictionary.end())
	{
		if (it_find->second)
			return it_find->second->create_entry();
	}
	return nullptr;
}

item_entry_dic  g_item_entry_dic;

item::item()
{
	for (cmn::byte i = 0; i < MAX_ENTRY_COUNT; i++)
	{
		m_entrys[i] = 0;
	}

	m_entry_count = 0;
}

item::~item()
{
	destroy();
}

void item::on_equip(prop_character* _char)
{
	for (cmn::byte i = 0; i < m_entry_count; i++)
	{
		m_entrys[i]->on_equip(_char);
	}
}

void item::on_unequip(prop_character* _char)
{
	for (cmn::byte i = 0; i < m_entry_count; i++)
	{
		m_entrys[i]->on_unequip(_char);
	}
}

bool item::create()
{
	m_entry_count = 1;//rand_range(1, 2);

	for (cmn::byte i = 0; i < m_entry_count; i++)
	{
		int begin	= g_item_entry_dic.class_list[i].begin;
		int end		= g_item_entry_dic.class_list[i].end-1;

		begin = ie_attrib_strength;
		end = ie_attrib_dexterity;

		item_entry_type it = (item_entry_type)rand_range((int)begin, (int)end);
		m_entrys[i] = g_item_entry_dic.create_entry(it,1);
	}
	return true;
}

bool item::load()
{
	return false;
}

void item::destroy()
{
	for (cmn::byte i = 0; i < m_entry_count; i++)
	{
		m_entrys[i]->release();
		m_entrys[i] = 0;
	}
	m_entry_count = 0;
}

#pragma warning(pop)
